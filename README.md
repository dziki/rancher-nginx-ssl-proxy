# SSL Proxy for Rancher
## Using Let's Encrypt & Nginx

### Usage

Run your rancher server:

```
docker run -d -v /opt/rancher/mysql:/var/lib/mysql --restart=unless-stopped rancher/server --name rancher-server:stable

```

And then run nginx proxy:

```
docker run -d -v certs:/etc/letsencrypt --restart=unless-stopped --link rancher-server -p 8080:8080 registry.gitlab.com/dziki/rancher-nginx-ssl-proxy:master
```

That's all! If only `certs` volume maintaned by `janeczku/rancher-letsencrypt` container will contains Let's Encrypt certificates files, Rancher will be available with encrypted `https` protocol under `pt5.pl:8080`.