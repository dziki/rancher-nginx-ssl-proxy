FROM nginx:1

LABEL maintainer="Michał Mleczko <michal@mleczko.waw.pl>"

COPY default.conf /etc/nginx/conf.d/default.conf

# Setup cron
RUN apt-get update && \
    apt-get install cron -y && \
    apt-get clean && \
# Setup the cron job
    echo "11 3 */2 * * root nginx -s reload" >> /etc/crontab && \
    rm -r /var/lib/apt/lists/*

# Setup cron to automatically re-load the NGINX configuration daily.
CMD cron && nginx -g "daemon off;"